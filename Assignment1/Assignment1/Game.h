#pragma once
#ifndef GAME_H 
#define GAME_H

#include <time.h>
#include <list>
#include "Enemy.h"
#include "Player.h"
#include "GameBoard.h"
#include <memory>
#include "SoundEffectsManager.h"
#include <regex>
#include "BasicProximityAI.h"

class Game
{

public:
	Game();
	~Game();

	void init(); // create the objects and assign each enemy and player unique typeID, health and x and y coords max health should be 160, speed is between 1 and 4
		 // place the objects on the list
	void draw(); // cycle through the enemy and player objects and call the draw function for each object
	void update(); //cycle through the enemy and player objects and call the update function for each object
	void battle(); // if two objects occupy the same coords declare a battle must take  place. The object with the higher health wins. The loser will have its health set to 0.
	void info(); // cycle through the enemy and player objects and call the info function for each object
	void clean(); //remove any objects from the list whose health is 0 
	void displayBoard();

	bool isRunning();
	
private:
	void assignSafeSpace(pair <int, int> playerPosition, int range);
	void generateEnemies();
	void configurePlayer();
	void checkEnemyActions(list<GameObject*>::iterator it);
	void checkPlayerActions(list<GameObject*>::iterator it);
	void isEnemyHit(vector < pair<int, int>> hitScan);
	void enemyKilled();

	int getNumEnemiesInput();

	bool m_enemyKilled;
	bool m_isGameRunning;
	unique_ptr<GameBoard> m_pGameBoard;
	unique_ptr<BasicProximityAI> m_pBasicProximityAI;

	pair <int, int> genRandUniqueCoords();
	static vector<pair<int, int>> m_vUniqueXYCoordinates;
	list<GameObject*> vpGameObjects;
};

#endif
