#pragma once

#include "SoundEffectsManager.h"

SoundEffectsManager* SoundEffectsManager::s_pInstance = 0;

void SoundEffectsManager::playSoundEffect(int freq, int duration)
{
	Beep(freq, duration);
}

SoundEffectsManager::~SoundEffectsManager()
{
	// Part of the Singleton design pattern is that it is indestructible.
}

