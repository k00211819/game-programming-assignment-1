#include "Game.h"

using namespace std;

int main()
{
	// Story and Instructions.
	cout << "\n\t Welcome Recruit! \n" << endl;
	cout << " You've entered the training grounds of Z++ Industries.\n" << endl;
	cout << " I understand you're here to get some practice in against the dangling pointer Zombies, eh? \n" << endl;

	cout << " Well, here's your Combat Shotgun. You can shoot 3 tiles wide and across OR you can move 1 tile per turn. \n" << endl;
	cout << " While the Zombies can't find you they will dash around 2 tiles per turn.\n" << endl;
	cout << " If they catch wind of you they will slow down and begin the hunt, to 1 tile per turn.\n" << endl;

	cout << " Mind your blind spots. At close ranges they will dash around, dodging your shots.\n" << endl;

	cout << " Make sure to line up em at a good distance and then... SPLATT!\n\n" << endl;

	cout << " Be careful though, son. If they manage to reach you, you're done for." << endl;

	cout << "\n Happy hunting Soldier." << endl;


	srand(time(NULL)); // Seed random.

	const int maxGameLoops = 20;

	bool isPlayerStillPlaying = true; // Track the state of the Player.

	while (isPlayerStillPlaying)
	{
		unique_ptr<Game> pG = 0;
		pG = make_unique<Game>();

		pG->init(); // Initialise the main components.

		int gameLoop = 0;

		// Main Game Loop
		while (pG->isRunning() && gameLoop < maxGameLoops) // While the game state is in active play and not above the max turn limit.
		{
			cout << " Turns taken: " << gameLoop << endl;

			//pG->draw(); // Draw some stats to the screen.
			pG->info(); // Draw some stats to the screen.
			pG->update(); // Update the entities.
			pG->battle(); // Kill the Player if a Zombie reaches him.
			pG->clean(); // Release our unneeded resources.

			gameLoop++; // Increment the Game Loop.
		}

		pG->info(); // Repeat info one last time for this loop.

		pG.reset(); // Delete the Game object, freeing the heap memory.
		pG = 0; // Deal with the dangling pointer.

		string repeatPlayInput = "";

		// End Game Lose Condition 1
		if (gameLoop == maxGameLoops)
		{
			cout << "\n\nSOLDIER! Your time is up. You gotta get them down faster!\n" << endl;
		}

		cout << "\n\tDo you wish to play again?\n\n\tN or n for \"No\", anything else for \"Yes\"." << endl;

		cout << "\t";
		cin >> repeatPlayInput; // Get choice from Player.

		if (repeatPlayInput == "N" || repeatPlayInput == "n")
		{
			isPlayerStillPlaying = false;
		}

		cin.clear();
		cin.ignore();
	}

	char finish;

	cout << "\n\n\tPlease enter any character to exit..." << endl;
	cout << "\t";
	cin >> finish;

	cout << "\n\t\tThanks for playing!\n\n" << endl;

	cout << "\nExiting Z++ Industries Simulation Program..." << endl;
	
	Sleep(5000);
	return 0;
}