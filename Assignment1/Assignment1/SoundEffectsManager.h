#pragma once
#ifndef SOUNDEFFECTSMANAGER_H 
#define SOUNDEFFECTSMANAGER_H
#include <windows.h> 

class SoundEffectsManager
{
public:

	static SoundEffectsManager* Instance()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new SoundEffectsManager();
			return s_pInstance;
		}

	return s_pInstance;
	}

	void playSoundEffect(int freq, int duration);


private:

	static SoundEffectsManager* s_pInstance;
	SoundEffectsManager() {} // Will not compile without these curly braces, why?

	~SoundEffectsManager();
};

typedef SoundEffectsManager TheSoundEffectsManager;

#endif