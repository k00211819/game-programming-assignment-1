#include "Player.h"


Player::Player() 
{
	m_healthDrain = 2;
}

Player::~Player()
{
}

void Player::update()
{
	controlManualAction();

	const int deductedHealth = m_speed * m_healthDrain;

	m_health -= deductedHealth;
}

// User tells the player what to do.
void Player::controlManualAction()
{
	string userActionInput = "";
	bool actionConfirmed;

	do
	{
		cout << endl << "\tPlease enter: M to MOVE or S to SHOOT." << endl;
		cout << "\t";
		cin >> userActionInput;

		actionConfirmed = true;

		if (userActionInput == "M" || userActionInput == "m") // Move
		{
			m_isShooting = false;
			movePlayer();
		}
		else if (userActionInput == "S" || userActionInput == "s") // Shoot
		{
			m_isShooting = true;
			m_vShotDetails = shoot();
		}
		else
		{
			actionConfirmed = false;
			cout << "Invalid input." << endl;
		}
	} while (!actionConfirmed);

	
}

/*	In the next assignment this code will be refactored to deliver a much cleaner function. much more readable and maintainable function.
	I plan to reuse a single variable that can easily adjust to the direction they are shooting. */
vector<pair<int, int>> Player::shoot() // User has told the Player to shoot.
{
	string userShootInput = "";
	bool isShootInputValid = true;

	vector<pair<int, int>> bulletSpread;
	const int shotXRange = 2;
	const int shotYRange = 2;

	// Very messy implementation of the coordinates needed to place bullets. TODO Clean this up.
	do
	{
		isShootInputValid = true;

		cout << endl << "\tPlease enter: W for SHOOT UP, A for SHOOT LEFT, S for SHOOT DOWN or D for SHOOT RIGHT." << endl;
		cout << "\t";
		cin >> userShootInput;

		if (userShootInput == "W" || userShootInput == "w") // 
		{
			for (int i = m_x - 1; i < (m_x + shotXRange); i++) // Shoot Across 3 x, centered on Player.
			{
				for (int j = m_y + 1; j <= (m_y + shotYRange+1); j++) // Shoot Up 3 y, centered on Player.
				{
					if ((i <= MAXCOORDVALUE) && (j <= MAXCOORDVALUE))
					{
						if ((i >= MINCOORDVALUE) && (j >= MINCOORDVALUE))
						{
							bulletSpread.push_back(make_pair(i, j));
						}
					}
				}
			}
		}
		else if (userShootInput == "S" || userShootInput == "s")
		{
			for (int i = m_x - 1; i < (m_x + shotXRange); i++) // Shoot Across 3 x, centered on Player.
			{
				for (int j = m_y - shotXRange - 1; j < (m_y); j++)  // Shoot Down 3 y, centered on Player.
				{
					if ((i <= MAXCOORDVALUE) && (j <= MAXCOORDVALUE))
					{
						if ((i >= MINCOORDVALUE) && (j >= MINCOORDVALUE))
						{
							bulletSpread.push_back(make_pair(i, j));
						}
					}
				}
			}
		}
		else if (userShootInput == "A" || userShootInput == "a")
		{
			for (int i = m_x - shotXRange-1; i <= (m_x-1); i++) // Shoot Across and down 3 x, centered on Player.
			{
				for (int j = m_y - 1; j < (m_y + shotYRange); j++) // Shoot Right 3 y, centered on Player.
				{
					if ((i <= MAXCOORDVALUE) && (j <= MAXCOORDVALUE))
					{
						if ((i >= MINCOORDVALUE) && (j >= MINCOORDVALUE))
						{
							bulletSpread.push_back(make_pair(i, j));
						}
					}
				}
			}
		}
		else if (userShootInput == "D" || userShootInput == "d")
		{
			for (int i = m_x + 1; i <= (m_x + shotXRange+1); i++) // Shoot Across and down 3 x, centered on Player.
			{
				for (int j = m_y - 1; j < (m_y + shotYRange); j++) // Shoot Left 3 y, centered on Player.
				{
					if ((i <= MAXCOORDVALUE) && (j <= MAXCOORDVALUE))
					{
						if ((i >= MINCOORDVALUE) && (j >= MINCOORDVALUE))
						{
							bulletSpread.push_back(make_pair(i, j));
						}
					}
				}
			}
		}
		else
		{
			cout << " Invalid input" << endl;
			isShootInputValid = false;
		}
	} while (!isShootInputValid);

	return bulletSpread;
}
// Player returns where he shot.
vector<pair<int, int>> Player::getShotDetails()
{
	return m_vShotDetails;
}
// User has told the Player to move.
void Player::movePlayer()
{
	string userMoveInput = "";
	bool isMoveInputValid;

	do
	{
		isMoveInputValid = true;

		cout << endl << "\tPlease enter: W for UP, A for LEFT, S for DOWN or D for RIGHT" << endl;
		cout << "\t";
		cin >> userMoveInput;

		if (userMoveInput == "W" || userMoveInput == "w") 
		{
			if ((m_y + m_speed) > MAXCOORDVALUE)
			{
				m_y = MAXCOORDVALUE; // Hit a wall.
			}
			else
			{
				m_y += m_speed; // Go Up.
			}
		}
		else if (userMoveInput == "A" || userMoveInput == "a")
		{
			if ((m_x - m_speed) < MINCOORDVALUE)
			{
				m_x = MINCOORDVALUE;  // Hit a wall.
			}
			else
			{
				m_x -= m_speed;  // Go Left.
			}

		}
		else if (userMoveInput == "S" || userMoveInput == "s")
		{
			if ((m_y - m_speed) < MINCOORDVALUE)
			{
				m_y = MINCOORDVALUE; // Hit a wall.
			}
			else
			{ 
				m_y -= m_speed;  // Go Down
			}

		}
		else if (userMoveInput == "D" || userMoveInput == "d")
		{
			if ((m_x + m_speed) > MAXCOORDVALUE)
			{
				m_x = MAXCOORDVALUE; // Hit a wall.
			}
			else
			{
				m_x += m_speed; // GoRight.
			}
		}
		else
		{
			cout << " Invalid input" << endl;
			isMoveInputValid = false;
		}
	} while (!isMoveInputValid);
}

// Not yet implemented for this Player Class.
void Player::controlAutoMovement(pair<int, int>)
{
	// Empty for now. If the player ever moves from something other than input I will use this.
}